#Name: Sri Padala

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import FeatureUnion



def display_scores(scores):
    print("Root mean square error:", scores)
    print("Mean:", scores.mean())
    print("Standard deviation:", scores.std())

def linear_regression(X_train, Y_train):
    print("Linear Regression")
    lin_reg = LinearRegression()
    lin_reg.fit(X_train, Y_train)

    housing_predictions = lin_reg.predict(X_train)
    lin_mse = mean_squared_error(Y_train, housing_predictions)
    lin_rmse = np.sqrt(lin_mse)

    scores = cross_val_score(lin_reg, X_train, Y_train,
    scoring="neg_mean_squared_error", cv=10)
    rmse_scores = np.sqrt(-scores)
    display_scores(rmse_scores)


def decision_tree_regressor(X_train, Y_train):
    print("Decision tree Regression")
    tree_reg = DecisionTreeRegressor()
    tree_reg.fit(X_train, Y_train)


    housing_predictions = tree_reg.predict(X_train)
    tree_mse = mean_squared_error(Y_train, housing_predictions)
    tree_rmse = np.sqrt(tree_mse)

    scores = cross_val_score(tree_reg, X_train, Y_train,
    scoring="neg_mean_squared_error", cv=10)
    rmse_scores = np.sqrt(-scores)
    display_scores(rmse_scores)


def random_forest_regressor(X_train, Y_train):
    print("Random forest Regression")
    forest_reg = RandomForestRegressor()
    forest_reg.fit(X_train, Y_train)

    housing_predictions = forest_reg.predict(X_train)
    tree_mse = mean_squared_error(Y_train, housing_predictions)
    tree_rmse = np.sqrt(tree_mse)

    scores = cross_val_score(forest_reg, X_train, Y_train,
    scoring="neg_mean_squared_error", cv=10)
    rmse_scores = np.sqrt(-scores)
    display_scores(rmse_scores)

def support_vector_regressor(X_train, Y_train):
    print("Support Vector Regression")
    svr = SVR(kernel="rbf", gamma=0.01)
    svr.fit(X_train, Y_train)

    housing_predictions = svr.predict(X_train)
    tree_mse = mean_squared_error(Y_train, housing_predictions)
    tree_rmse = np.sqrt(tree_mse)

    scores = cross_val_score(svr, X_train, Y_train,
    scoring="neg_mean_squared_error", cv=10)
    rmse_scores = np.sqrt(-scores)
    display_scores(rmse_scores)

def grid_search(X_train, Y_train, test_set, num_pipeline):
    print("Deploying Grid Search on Random forest Regression")
    param_grid = [
    {'n_estimators': [10, 20, 30, 40], 'max_features': [4, 8]},
    ]
    forest_reg = RandomForestRegressor()
    grid_search = GridSearchCV(forest_reg, param_grid, cv=10,
    scoring='neg_mean_squared_error')
    grid_search.fit(X_train, Y_train)

    cvres = grid_search.cv_results_
    for mean_score, params in zip(cvres["mean_test_score"], cvres["params"]):
        print(np.sqrt(-mean_score), params)


    final_model = grid_search.best_estimator_
    print("\nBest model Hyper parameter values for the best estimator")
    print(final_model)
    X_test = test_set.drop(13, axis=1)
    y_test = test_set[13].copy()

    full_pipeline = FeatureUnion(transformer_list=[
    ("num_pipeline", num_pipeline),
    ])
    X_test_prepared = full_pipeline.transform(X_test)

    final_predictions = final_model.predict(X_test_prepared)
    final_mse = mean_squared_error(y_test, final_predictions)
    final_rmse = np.sqrt(final_mse)
    print("\nUsing the best estimator the RMSE for the Test SET  is: ",final_rmse )

def main():
    housing = pd.read_csv('data/boston_housing.data', header=None,sep=" " )

    housing[13] = housing[13] * 1000

    train_set, test_set = train_test_split(housing, test_size=0.3, random_state=42) 

    X_train = train_set.drop(13, axis=1)
    Y_train = train_set[13].copy()
    X_test = test_set.drop(13, axis=1)
    Y_test = test_set[13].copy()

    num_pipeline = Pipeline([
    ('imputer', SimpleImputer(strategy="median")),
    ('std_scaler', StandardScaler()),
    ])

    X_train = num_pipeline.fit_transform(X_train)
    X_test = num_pipeline.transform(X_test)

    linear_regression(X_train, Y_train)
    print("\n")
    decision_tree_regressor(X_train, Y_train)
    print("\n")
    random_forest_regressor(X_train, Y_train)
    print("\n")
    support_vector_regressor(X_train, Y_train)
    print("\n")
    grid_search(X_train, Y_train, test_set, num_pipeline)

if __name__ == "__main__":
    main()


