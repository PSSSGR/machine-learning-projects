#Name: Sri Padala

from sklearn.datasets import fetch_openml
from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score
from matplotlib import pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.model_selection import train_test_split
from warnings import simplefilter


def plot_roc_curve(fpr, tpr, label=None):
    plt.plot(fpr, tpr, linewidth=2, label=label)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.axis([0, 1, 0, 1])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')

def main():
    mnist = fetch_openml('mnist_784', version=1, cache=True)
    X, Y = mnist["data"], mnist["target"]
    Y = Y.astype(int)

    X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size=0.2, random_state=43)
    Y_train = (Y_train == 8)
    Y_test = (Y_test == 8)

    num_pipeline = Pipeline([
    ('imputer', SimpleImputer(strategy="median")),
    ('std_scaler', StandardScaler()),
    ])

    X_train_tr = num_pipeline.fit_transform(X_train)
    X_test_tr = num_pipeline.transform(X_test)

    log_reg = LogisticRegression()
    log_reg.fit(X_train_tr, Y_train)
    scores = cross_val_score(log_reg, X_train_tr, Y_train, cv=3, scoring="accuracy")
    y_train_pred = cross_val_predict(log_reg, X_train_tr, Y_train, cv=3)

    print("Confision Matrix:")
    print(confusion_matrix(Y_train, y_train_pred))
    print("\n")
    print("Precision Score:")
    print(precision_score(Y_train, y_train_pred))
    print("\n")
    print("Recall Score:")
    print(recall_score(Y_train, y_train_pred))
    print("\n")

    Y_scores = cross_val_predict(log_reg, X_train_tr, Y_train, cv=3, method="predict_proba")
    Y_scores = Y_scores[:, 1]
    
    fpr, tpr, thresholds = roc_curve(Y_train,Y_scores)

    plot_roc_curve(fpr, tpr, "Logistic Regression")
    plt.legend(loc="best")
    plt.show()

    Y_scores_classifier = cross_val_predict(log_reg, X_train_tr, Y_train, cv=3, method="predict_proba")
    Y_scores_classifier = Y_scores_classifier[:, 1] > 0.99
    print("Precision Score using designed classifier:")
    print(precision_score(Y_train, Y_scores_classifier))
    print("\n")
    print("Recall Score using designed classifier:")
    print(recall_score(Y_train, Y_scores_classifier))
    print("\n")

    log_reg2 = LogisticRegression()
    log_reg2.fit(X_test_tr, Y_test)
    Y_scores_classifier2 = cross_val_predict(log_reg2, X_test_tr, Y_test, cv=3, method="predict_proba")
    Y_scores_classifier2 = Y_scores_classifier2[:, 1] > 0.99
    print("Precision Score using designed classifier on test set:")
    print(precision_score(Y_test, Y_scores_classifier2))
    print("\n")
    print("Recall Score using designed classifier on test set:")
    print(recall_score(Y_test, Y_scores_classifier2))
    
    

if __name__ == "__main__":
    simplefilter(action='ignore', category=FutureWarning)
    main()
