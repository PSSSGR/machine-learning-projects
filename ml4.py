#Name: Sri Padala

from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
from warnings import simplefilter

def main():
    X, y = load_digits(return_X_y=True)

    X_train, X_test, Y_train, Y_test = train_test_split(X,y, test_size=0.4, random_state=43)

    Y_train = [val == 8 or val == 5 for val in Y_train]
    Y_test = [val == 8 or val == 5 for val in Y_test]


    num_pipeline = Pipeline([
        ('imputer', SimpleImputer(strategy="median")),
        ('std_scaler', StandardScaler()),
        ])

    X_train_tr = num_pipeline.fit_transform(X_train)
    X_test_tr = num_pipeline.transform(X_test)


    param_grid = [
        {'n_estimators': [100, 200, 300, 400],},
        ]
    forest_clf = RandomForestClassifier()
    grid_search = GridSearchCV(forest_clf, param_grid, cv=3, scoring='roc_auc')

    grid_search.fit(X_train_tr, Y_train)

    cvres = grid_search.cv_results_
    for mean_score, params in zip(cvres["mean_test_score"], cvres["params"]):
        print(mean_score, params)

    final_model = grid_search.best_estimator_
    print("\nBest model Hyper parameter values for the best estimator")
    print(final_model)

    final_model.fit(X_train_tr, Y_train)

    final_predictions = final_model.predict(X_test_tr)
    final_auc_score = roc_auc_score(Y_test, final_predictions)
    print("\nUsing the best estimator the AUC score for the Test SET  is: ",final_auc_score )

    pca = PCA()
    X_train_reduced = pca.fit_transform(X_train_tr)
    d_list = []
    test_auc_list = []
    for d in range(2,65,2):
        grid_search.fit(X_train_reduced[:,0:d],Y_train)
        cvres = grid_search.cv_results_
        for score, params in zip(cvres['mean_test_score'],cvres['params']):
            print(score,'d =',d,params)
        print('Number of Principal components used')
        print('d = ',d)
        final_reduced_model = grid_search.best_estimator_
        print(final_reduced_model) 
        X_test_reduced = X_test_tr.dot(pca.components_.T[:,0:d])
        final_predictions = final_reduced_model.predict(X_test_reduced)
        final_AUC = roc_auc_score(Y_test,final_predictions)
        print('AUC score : ',final_AUC)
        d_list.append(d)
        test_auc_list.append(final_AUC)
    #printing the graph
    plt.plot(d_list,test_auc_list)
    plt.xlabel('Reduced dimension')
    plt.ylabel('Test ROC')
    plt.show()


if __name__ == "__main__":
    simplefilter(action='ignore', category=FutureWarning)
    main()