#Name: Sri Padala


from sklearn.datasets import load_digits
from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score
from matplotlib import pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import roc_auc_score
from warnings import simplefilter


def main():
    X, y = load_digits(return_X_y=True)
    X_train, X_test, Y_train, Y_test = train_test_split(X,y, test_size=0.4, random_state=43)
    Y_train = [val == 8 or val == 5 for val in Y_train]
    Y_test = [val == 8 or val == 5 for val in Y_test]

    num_pipeline = Pipeline([
    ('imputer', SimpleImputer(strategy="median")),
    ('std_scaler', StandardScaler()),
    ])

    X_train_tr = num_pipeline.fit_transform(X_train)
    X_test_tr = num_pipeline.transform(X_test)

    svc = SVC()
    parameters = [{'kernel':['linear'], 'C':[0.01,0.1,1,10,100]},
              {'kernel':['rbf'],'gamma':['auto'], 'C':[0.01,0.1,1,10,100]},
              {'kernel':['poly'], 'gamma':['auto'],'degree':[2,4,6],'C':[0.01,0.1,1,10,100]},]
    print("Deploying Grid Search")
    grid_search = GridSearchCV(svc, parameters, cv =3, scoring = 'roc_auc')
    grid_search.fit(X_train_tr, Y_train)

    cvres = grid_search.cv_results_
    for mean_score, params in zip(cvres["mean_test_score"], cvres["params"]):
        print(mean_score, params)
    
    final_model = grid_search.best_estimator_
    print("\nBest model Hyper parameter values for the best estimator")
    print(final_model)

    final_model.fit(X_train_tr, Y_train)

    final_predictions = final_model.predict(X_test_tr)
    final_auc_score = roc_auc_score(Y_test, final_predictions)
    print("\nUsing the best estimator the AUC score for the Test SET  is: ",final_auc_score )
    

if __name__ == "__main__":
    simplefilter(action='ignore', category=FutureWarning)
    main()
